import React, { useEffect } from "react";
import ReactDOM from "react-dom";
import {
  BrowserRouter,
  Routes,
  Route,
  useNavigate,
  useLocation,
} from "react-router-dom";
import "./mf.d.ts";
import Login from "authentication/login";
import Info from "information/info";

import "./index.scss";

const App = () => {
  const navigate = useNavigate();
  const location = useLocation();

  useEffect(() => {
    const data = JSON.parse?.(sessionStorage.getItem("data") as string);
    if (data && location.pathname !== "/info") navigate("/info");
    else if (!data) navigate("/");
  }, []);

  return (
    <Routes>
      <Route path="/" element={<Login onSubmit={() => navigate("/info")} />} />
      <Route path="/info" element={<Info onLogOut={() => navigate("/")} />} />
    </Routes>
  );
};

ReactDOM.render(
  <BrowserRouter>
    <App />
  </BrowserRouter>,
  document.getElementById("containerRoot")
);
